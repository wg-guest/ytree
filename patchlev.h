/***************************************************************************
 *
 * $Header: /usr/local/cvsroot/utils/ytree/patchlev.h,v 1.53 2016/09/04 17:15:34 werner Exp $
 *
 * Patchlevel-Definitionen
 *
 ***************************************************************************/


#define	VERSION		"1.99"
#define	PATCHLEVEL	1
#define VERSIONDATE	"Sep 04 2016"
